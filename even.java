import java.util.Scanner;
public class Even
{
    public static void main(String[] args)
    {
        Scanner sc=new Scanner(System.in);
        int num;
        System.out.println("Enter a positive numbergreater than 0 and less than 10000");
        num=sc.nextInt();
        if ( num<1 || num > 10000)
        {
            System.out.println("Invalid Input");
        }
        else
        {
            if(num%2==0)
            {
                System.out.println("Even number");
            }
            else
            {
                System.out.println("Odd number");
            }
        }
    }

}